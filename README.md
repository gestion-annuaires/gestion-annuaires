# Gestionnaire d’annuaires

Le gestionnaire d’annuaires est un outil en ligne de commande permettant de manipuler des fichiers représentant un annuaire d’abonnements.

## Installation

Ce projet est écrit en <cite>C</cite>.

## Démarrer

Compilez avec `make main` et ouvrez votre console puis lancez `./target/gestion-annuaires`.

### Conteneurisation

Pour créer un conteneur, utilisez l’utilitaire <cite>Make</cite> :
* `make build run`

En tant que superutilisateur avec <cite>Docker</cite>, ou simplement avec <cite>Podman</cite>.

Vous pouvez configurer les paramètres de l’image et du conteneur dans le fichier `.env` ou les écraser directement en ligne de commande en les ajoutant à la fin (par exemple `make run VOLUME=$HOME/dossier/local`). Utilisez `$HOME` au lieu de `~`, car ce symbole n’est pas traduit dans les commandes.

Les fichiers auxquels vous faites référence en interagissant avec le programme sont relatifs au dossier indiqué dans le paramètre `VOLUME`.

Nettoyez votre environnement de travail avec `make clean`. Il vous incombe de supprimer les images intermédiaires gardées en cache, par exemple via `docker image prune`.

## Support et contributions

Ce projet ne sera pas maintenu et n’acceptera pas de contributions.

## Auteurs et contributeurs

Principalement écrit par Florian Monfort.

## License

Cette application est sous licence GPLv3.
