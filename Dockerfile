FROM alpine:latest AS builder
RUN apk add --no-cache gcc make musl-dev
RUN adduser -D cli
USER cli
WORKDIR /home/cli
COPY . .
RUN make main

FROM alpine:latest
RUN adduser -D cli
USER cli
WORKDIR /home/cli
COPY --from=builder /home/cli/target/gestion-annuaires /usr/local/bin/gestion-annuaires
ENTRYPOINT ["gestion-annuaires"]
