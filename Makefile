include .env

main:
	mkdir -p target
	gcc src/main.c -o target/gestion-annuaires

build:
	docker build -t $(IMAGE) .

run:
	docker run -it --rm --read-only -v $(VOLUME):/home/cli/ --workdir="/home/cli/" --name $(CONTENEUR) $(IMAGE)

clean:
	docker rmi $(IMAGE)
