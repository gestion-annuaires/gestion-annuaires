#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include "main.h"

/**********************************************/
/**************** UTILITAIRES *****************/
/**********************************************/

void flushStdin() {
	// Vide l’entrée standard pour préparer la prochaine saisie
	while (getchar() != '\n');
}

char* obtenirSaisie(int tailleLimiteSaisie) {
	// Tampon de stockage du nom de fichier fourni par l’utilisateur
	char* saisie = (char*) malloc(sizeof(char) * (tailleLimiteSaisie + 1));

	if (saisie == NULL) {
		perror("Pas assez de mémoire disponible. Le programme doit terminer.\n");
		exit(EXIT_FAILURE);
	}

	do {
		// Lit la donnée et vérifie le cas d’erreur
		if (fgets(saisie, tailleLimiteSaisie + 1, stdin) == NULL) {
			perror("Impossible de lire depuis l’entrée standard. Le programme doit terminer.\n");
			exit(EXIT_FAILURE);
		}

		int positionRetourLigne = strcspn(saisie, "\n");
		if (positionRetourLigne == 0) {
			fprintf(stderr, "Votre saisie est vide, veuillez recommencer : ");
		}

		else if (positionRetourLigne == tailleLimiteSaisie) {
			flushStdin();
			fprintf(stderr, "Votre saisie est trop longue, veuillez recommencer : ");
		}

		// La saisie est valide
		else {
			// Remplace le dernier caractère (retour à la ligne) par une fin de chaine
			saisie[strcspn(saisie, "\n")] = '\0';
			break;
		}
	}
	while (true);

	return saisie;
}

int obtenirNombre() {
	long nombre;
	char* saisie;
	bool nombreValide;
	char* pointeurFinConversion;

	do {
		// Un int se note sur 5 chiffres
		saisie = obtenirSaisie(5);

		nombre = strtol(saisie, &pointeurFinConversion, 10);

		if (errno == ERANGE || nombre < 0 || nombre > INT_MAX) {
			nombreValide = false;
			fprintf(stderr, "Entrée invalide, veuillez recommencer : ");
		}
		else {
			// La saisie doit contenir au moins un chiffre et se terminer par un \0 ou \n
			nombreValide = pointeurFinConversion != saisie && (*pointeurFinConversion == '\0' || *pointeurFinConversion == '\n');
		}
	}
	while (!nombreValide);

	return nombre;
}

void permuter(abonne* abonne1, abonne* abonne2) {
	abonne coureurTemporaire = *abonne1;
	*abonne1 = *abonne2;
	*abonne2 = coureurTemporaire;
}

void maintenirTrie(abonne* tableauAbonnes, int n) {
	int i = n;

	while (i && strncmp(tableauAbonnes[i - 1].nom, tableauAbonnes[i].nom, LONGUEUR_NOM_ABONNE) > 0) {
		permuter(&tableauAbonnes[i - 1], &tableauAbonnes[i]);
		i--;
	}
}

/**********************************************/
/***************** FONCTIONS ******************/
/**********************************************/

int lireFichier(char* nomFichier, abonne* tableauAbonnes) {
	FILE* fichier = fopen(nomFichier, "rb");
	int i = 0;

	if (fichier == NULL) {
		fprintf(stderr, "Impossible d’ouvrir ce fichier. Vérifiez qu’il existe et que vous avez les droits.\n");
		return -1;
	}

	i = fread(tableauAbonnes, sizeof(abonne), NOMBRE_MAX_ABONNES, fichier);
	fclose(fichier);

	return i;
}

void ecrireFichier(char* nomFichier, abonne* tableauAbonnes, int n) {
	FILE* fichier = fopen(nomFichier, "wb");

	if (fichier == NULL) {
		fprintf(stderr, "Impossible d’ouvrir ce fichier. Vérifiez qu’il existe et que vous avez les droits.\n");
		return;
	}

	fwrite(tableauAbonnes, sizeof(abonne), n, fichier);
	fclose(fichier);
}

/**********************************************/
/***************** COMMANDES ******************/
/**********************************************/

void afficherAnnuaire() {
	printf("Nom du fichier à afficher : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);
	printf("\n");

	FILE* fichier = fopen(nomFichier, "rb");

	if (fichier == NULL) {
		fprintf(stderr, "Impossible d’ouvrir ce fichier. Vérifiez qu’il existe et que vous avez les droits.\n");
		return;
	}

	// Vérifie si le fichier est vide (place le curseur à la fin et regarde si sa position vaut 0)
	fseek(fichier, 0, SEEK_END);
	if (ftell(fichier) == 0) {
		fprintf(stderr, "Ce fichier est vide.\n");
		return;
	}

	int elementsLus;
	abonne abonneCourant;
	// Format dd/mm/yyyy
	char dateCreationAbonnement[11];
	char dateExpirationAbonnement[11];

	// Puis remet le curseur au début pour parcourir les entrées
	fseek(fichier, 0, SEEK_SET);
	do {
		elementsLus = fread(&abonneCourant, sizeof(abonneCourant), 1, fichier);

		if (elementsLus == 1) {
			time_t tsCreation = abonneCourant.tsCreationAbonnement;
			struct tm* dateCreation = localtime(&tsCreation);
			strftime(dateCreationAbonnement, 11, "%d/%m/%Y", dateCreation);

			time_t tsExpiration = abonneCourant.tsCreationAbonnement + abonneCourant.dureeAbonnement * SECONDES_PAR_JOUR;
			struct tm* dateExpiration = localtime(&tsExpiration);
			strftime(dateExpirationAbonnement, 11, "%d/%m/%Y", dateExpiration);

			printf("Nom : %s\n", abonneCourant.nom);
			printf("Numéro : %s\n", abonneCourant.numero);
			printf("Date de création de l’abonnement : %s\n", dateCreationAbonnement);
			printf("Date d’expiration de l’abonnement : %s\n", dateExpirationAbonnement);
			printf("\n");
		}
	}
	while (!feof(fichier));

	fclose(fichier);
}

void ajouterAbonne() {
	printf("Nom du fichier dans lequel ajouter des informations (crée l’annuaire s’il n’existe pas) : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);
	printf("\n");

	FILE* fichier = fopen(nomFichier, "ab");

	if (fichier == NULL) {
		fprintf(stderr, "Impossible d’ouvrir ce fichier. Vérifiez qu’il existe et que vous avez les droits.\n");
		return;
	}

	printf("\n");
	printf("Vous allez entrer les informations sur les nouveaux abonnés.\n");
	printf("Entrez %s pour finir la saisie.\n", FIN_SAISIE);

	abonne abonneEntre;

	do {
		printf("\n");
		printf("Nom de l’abonné : ");
		strncpy(abonneEntre.nom, obtenirSaisie(LONGUEUR_NOM_ABONNE), LONGUEUR_NOM_ABONNE);

		if (strncmp(abonneEntre.nom, FIN_SAISIE, LONGUEUR_NOM_ABONNE) == 0) {
			break;
		}

		printf("Numéro de l’abonné : ");
		strncpy(abonneEntre.numero, obtenirSaisie(LONGUEUR_NUMERO_ABONNE), LONGUEUR_NUMERO_ABONNE);

		printf("Durée de son abonnement (en jours) : ");
		abonneEntre.dureeAbonnement = obtenirNombre();

		// Récupère le timestamp d’aujourd’hui à minuit
		time_t maintenant = time(NULL);
		struct tm* aujourdhui = localtime(&maintenant);
		aujourdhui->tm_hour = 0;
		aujourdhui->tm_min = 0;
		aujourdhui->tm_sec = 0;
		abonneEntre.tsCreationAbonnement = (long) mktime(aujourdhui);

		fwrite(&abonneEntre, sizeof(abonneEntre), 1, fichier);
	}
	while (true);

	fclose(fichier);

	printf("Le fichier a été sauvegardé.\n");
}

void trierFichierAnnuaire() {
	printf("Nom du fichier à trier : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);
	printf("\n");

	int nombreAbonnes;
	abonne tableauAbonnes[NOMBRE_MAX_ABONNES];

	if ((nombreAbonnes = lireFichier(nomFichier, tableauAbonnes)) == -1) {
		return;
	}

	if (nombreAbonnes == 0) {
		printf("Ce fichier est vide.\n");
		return;
	}

	for (int j = 1; j < nombreAbonnes; j++) {
		maintenirTrie(tableauAbonnes, j);
	}

	ecrireFichier(nomFichier, tableauAbonnes, nombreAbonnes);
	printf("Le fichier a été trié et sauvegardé.\n");
}

void chercherNumeroAbonne() {
	printf("Nom du fichier où chercher : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);

	FILE* fichier = fopen(nomFichier, "rb");
	if (fichier == NULL) {
		fprintf(stderr, "Impossible d’ouvrir ce fichier. Vérifiez qu’il existe et que vous avez les droits.\n");
		return;
	}

	// Vérifie si le fichier est vide (place le curseur à la fin et regarde si sa position vaut 0)
	fseek(fichier, 0, SEEK_END);
	if (ftell(fichier) == 0) {
		fprintf(stderr, "Ce fichier est vide.\n");
		return;
	}

	printf("Nom de l’abonné à chercher : ");
	char* nomAbonne = obtenirSaisie(LONGUEUR_NOM_ABONNE);
	printf("\n");

	abonne abonneEntre;
	bool abonneTrouve = false;
	char numeroAbonne[LONGUEUR_NUMERO_ABONNE];

	// Puis remet le curseur au début pour parcourir les entrées
	fseek(fichier, 0, SEEK_SET);
	do {
		fread(&abonneEntre, sizeof(abonneEntre), 1, fichier);

		if (strncmp(nomAbonne, abonneEntre.nom, LONGUEUR_NOM_ABONNE) == 0) {
			abonneTrouve = true;
			strncpy(numeroAbonne, abonneEntre.numero, LONGUEUR_NUMERO_ABONNE);
		}
	}
	while (!abonneTrouve || !feof(fichier));

	if (!abonneTrouve) {
		printf("L’abonné n’existe pas dans le registre.\n");
		return;
	}

	printf("L’abonné %s a le numéro %s.\n", nomAbonne, numeroAbonne);
}

void prolongerDureeTousAbonnements() {
	printf("Nom du fichier à modifier : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);

	int nombreAbonnes;
	abonne tableauAbonnes[NOMBRE_MAX_ABONNES];

	if ((nombreAbonnes = lireFichier(nomFichier, tableauAbonnes)) == -1) {
		return;
	}

	if (nombreAbonnes == 0) {
		printf("Ce fichier est vide.\n");
		return;
	}

	printf("Durée de la prolongation des abonnements (en jours) : ");
	int dureeAbonnementSupplementaire = obtenirNombre();
	printf("\n");

	for (int i = 0; i < nombreAbonnes; i++) {
		tableauAbonnes[i].dureeAbonnement += dureeAbonnementSupplementaire;
	}

	ecrireFichier(nomFichier, tableauAbonnes, nombreAbonnes);
	printf("Les durées d’abonnement de tous les abonnés ont été mises à jour.\n");
}

void prolongerDureeAbonnement() {
	printf("Nom du fichier à modifier : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);

	int nombreAbonnes;
	abonne tableauAbonnes[NOMBRE_MAX_ABONNES];

	if ((nombreAbonnes = lireFichier(nomFichier, tableauAbonnes)) == -1) {
		return;
	}

	if (nombreAbonnes == 0) {
		printf("Ce fichier est vide.\n");
		return;
	}

	printf("Nom de l’abonné à traiter : ");
	char* nomAbonne = obtenirSaisie(LONGUEUR_NOM_ABONNE);

	int i = 0;
	bool abonneTrouve = false;

	while (i < nombreAbonnes && !abonneTrouve) {
		if (strncmp(tableauAbonnes[i].nom, nomAbonne, LONGUEUR_NOM_ABONNE) == 0) {
			printf("Durée de la prolongation de son abonnement (en jours) : ");
			int dureeAbonnementSupplementaire = obtenirNombre();
			printf("\n");

			tableauAbonnes[i].dureeAbonnement += dureeAbonnementSupplementaire;
			abonneTrouve = true;
		}

		i++;
	}

	if (!abonneTrouve) {
		printf("L’abonné n’existe pas dans le registre.\n");
		return;
	}

	ecrireFichier(nomFichier, tableauAbonnes, nombreAbonnes);
	printf("La durée d’abonnement de %s a été mise à jour.\n", nomAbonne);
}

void supprimerAbonne() {
	printf("Nom du fichier à modifier : ");
	char* nomFichier = obtenirSaisie(LONGUEUR_NOM_FICHIER);

	int nombreAbonnes;
	abonne tableauAbonnes[NOMBRE_MAX_ABONNES];

	if ((nombreAbonnes = lireFichier(nomFichier, tableauAbonnes)) == -1) {
		return;
	}

	if (nombreAbonnes == 0) {
		printf("Ce fichier est vide.\n");
		return;
	}

	printf("Nom de l’abonné à supprimer : ");
	char* nomAbonne = obtenirSaisie(LONGUEUR_NOM_ABONNE);
	printf("\n");

	bool abonneTrouve = false;

	for (int i = 0; i < nombreAbonnes - 1; i++) {
		if (abonneTrouve || strncmp(tableauAbonnes[i].nom, nomAbonne, LONGUEUR_NOM_ABONNE) == 0) {
			tableauAbonnes[i] = tableauAbonnes[i + 1];
			abonneTrouve = true;
		}
	}

	// Si l’abonné recherché est le dernier, alors on le marque comme trouvé sans permuter de valeur
	abonneTrouve = abonneTrouve || strncmp(tableauAbonnes[nombreAbonnes - 1].nom, nomAbonne, LONGUEUR_NOM_ABONNE) == 0;

	if (!abonneTrouve) {
		printf("L’abonné n’existe pas dans le registre.\n");
		return;
	}

	ecrireFichier(nomFichier, tableauAbonnes, abonneTrouve ? nombreAbonnes - 1 : nombreAbonnes);
	printf("L’abonné %s a été supprimé du registre.\n", nomAbonne);
}

/**********************************************/
/******************** MAIN ********************/
/**********************************************/

int main(void) {
	int choix;

	// Pour que le conteneur Docker fonctionne correctement
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	do {
		do {
			printf("Liste des commandes :\n");
			printf("	1 : afficher un annuaire\n");
			printf("	2 : créer ou ajouter des abonnés à un annuaire\n");
			printf("	3 : trier un annuaire\n");
			printf("	4 : chercher un numéro d’abonné\n");
			printf("	5 : Prolonger les durées d’abonnement de tous les abonnés\n");
			printf("	6 : Prolonger la durée d’abonnement d’un abonné\n");
			printf("	7 : Supprimer un abonné\n");
			printf("	0 : quitter\n");
			printf("\nNuméro de commande : ");
			choix = obtenirNombre();

			if (choix < NUMERO_COMMANDE_MIN || choix > NUMERO_COMMANDE_MAX) {
				printf("Entrée incorrecte.\n");
			}
		}
		while (choix < NUMERO_COMMANDE_MIN || choix > NUMERO_COMMANDE_MAX);

		switch (choix) {
			case NUMERO_COMMANDE_SORTIE:
				printf("Au revoir.\n");
			break;

			case NUMERO_COMMANDE_AFFICHER_ANNUAIRE:
				afficherAnnuaire();
			break;

			case NUMERO_COMMANDE_AJOUTER_ABONNE:
				ajouterAbonne();
			break;

			case NUMERO_COMMANDE_TRIER_ANNUAIRE:
				trierFichierAnnuaire();
			break;

			case NUMERO_COMMANDE_CHERCHER_ABONNE:
				chercherNumeroAbonne();
			break;

			case NUMERO_COMMANDE_PROLONGER_DUREE_TOUS_ABONNEMENTS:
				prolongerDureeTousAbonnements();
			break;

			case NUMERO_COMMANDE_PROLONGER_DUREE_ABONNEMENT:
				prolongerDureeAbonnement();
			break;

			case NUMERO_COMMANDE_SUPPRIMER_ABONNE:
				supprimerAbonne();
			break;
		}
		printf("\n");
	}
	while (choix != NUMERO_COMMANDE_SORTIE);

	return EXIT_SUCCESS;
}
