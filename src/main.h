#ifndef MAIN_H
#define MAIN_H

#define SECONDES_PAR_JOUR 86400

#define NOMBRE_MAX_ABONNES 50
#define LONGUEUR_NOM_FICHIER 255
#define LONGUEUR_NOM_ABONNE 79
#define LONGUEUR_NUMERO_ABONNE 10

#define NUMERO_COMMANDE_MIN 0
#define NUMERO_COMMANDE_MAX 8
#define NUMERO_COMMANDE_SORTIE 0
#define NUMERO_COMMANDE_AFFICHER_ANNUAIRE 1
#define NUMERO_COMMANDE_AJOUTER_ABONNE 2
#define NUMERO_COMMANDE_TRIER_ANNUAIRE 3
#define NUMERO_COMMANDE_CHERCHER_ABONNE 4
#define NUMERO_COMMANDE_PROLONGER_DUREE_TOUS_ABONNEMENTS 5
#define NUMERO_COMMANDE_PROLONGER_DUREE_ABONNEMENT 6
#define NUMERO_COMMANDE_SUPPRIMER_ABONNE 7

#define FIN_SAISIE "*"

typedef struct {
	char nom[LONGUEUR_NOM_ABONNE];
	char numero[LONGUEUR_NUMERO_ABONNE];
	long tsCreationAbonnement;
	int dureeAbonnement;
} abonne;

#endif
